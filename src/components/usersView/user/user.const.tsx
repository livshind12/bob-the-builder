import { ColorType } from "./user.component";

export const userBordersColors:ColorType[] = [
    {
        angle:'135deg',
        color1:"#FDEB71",
        percentColorOne:"10%",
        color2:"#F8D800",
        percentColorTwo:" 100%"
    },
    {
        angle:'135deg',
        color1:"#ABDCFF",
        percentColorOne:"10%",
        color2:"#0396FF",
        percentColorTwo:" 100%"
    },
    {
        angle:'135deg',
        color1:"#FEB692",
        percentColorOne:"10%",
        color2:"#EA5455",
        percentColorTwo:" 100%"
    },
    {
        angle:'135deg',
        color1:"#CE9FFC",
        percentColorOne:"10%",
        color2:"#7367F0",
        percentColorTwo:" 100%"
    },
    {
        angle:'135deg',
        color1:"#F761A1",
        percentColorOne:"10%",
        color2:"#8C1BAB",
        percentColorTwo:" 100%"
    },
    {
        angle:'135deg',
        color1:"#FEC163",
        percentColorOne:"10%",
        color2:"#DE4313",
        percentColorTwo:" 100%"
    },
    {
        angle:'135deg',
        color1:"#FFF720",
        percentColorOne:"10%",
        color2:"#3CD500",
        percentColorTwo:" 100%"
    },
]

